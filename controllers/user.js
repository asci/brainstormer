var User = require("../models/user");

var app = require("../app");
 
module.exports = (function() {
  // GET /users
  // Получаем всех пользователей
  // Шаблон index
	app.get('/users', function(req, res){
		//if (!req.session.admin) res.redirect('/') 
		User.all(function (users) {
			res.render('users/index', {
				users:users, title:'Пользователи', layout:"../layout"
			})
		})
	});
  // GET /users/new
  // Создаем нового пользователя
  // Шаблон new
	app.get('/users/new', function(req, res){
		if (!req.session.admin) res.redirect('/') 
		User.new_(function (user) {
			res.render('users/new', {
				user:user, layout:"../layout"
			})
		})
	});
  // POST /users
  // Создаем пользователя в БД
  // Шаблон show
	app.post('/users', function(req, res){
		if (!req.session.admin) res.redirect('/') 
		User.create(req.body.user, function (user) {
			res.render('users/show', {
				user:user, layout:"../layout"
			})
		})
	});
  // GET /users/:link
  // Получаем пользователя и его данные
  // Шаблон show
	app.get('/users/:link', function(req, res){
		if (!req.session.admin) res.redirect('/') 
		User.find(req.params.link, function (user) {
			res.render('users/show', {
				user:user, layout:"../layout"
			})
		})
	});
  // GET /users/:link/edit
  // Выбираем пользователя для редакторивания
  // Шаблон show
	app.get('/users/:link/edit', function(req, res){
		if (!req.session.admin) res.redirect('/') 
		User.find(req.params.link, function (user) {
			res.render('users/edit', {
				user:user, layout:"../layout"
			})
		})
	});
  // PUT /users/:link
  // Обновляем пользователя в БД и показываем
  // Шаблон show
	app.put('/users/:link', function(req, res){
		if (!req.session.admin) res.redirect('/') 
		User.update(req.body.user, function (user) {
			res.render('users/show', {
				user:req.body.user, layout:"../layout"
			})
		})
	});
  // GET /users/:link/delete
  // Удаляем пользователя
  // Шаблон index
	app.get('/users/:link/delete', function(req, res){
		if (!req.session.admin) res.redirect('/') 
		User.destroy(req.params.link, function () {
			User.all(function (users) {
				res.render('users/index', {
					users:users, layout:"../layout"
				})
			})
		})
	});
}());
