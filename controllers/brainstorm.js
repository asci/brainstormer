var Brainstorm = require("../models/brainstorm");

var app = require("../app");
 
module.exports = (function() {
  // GET /brainstorms
  // Получаем все штормы текущего пользователя
  // Шаблон index
	app.get('/brainstorms', function(req, res){
		if (!req.user) res.redirect('/');
		if (req.session.admin) {
			Brainstorm.all(function (brainstorms) {
				res.render('brainstorms/index', {
					brainstorms:brainstorms, layout:"../layout"
				})
			})			
		} else {
			Brainstorm.findByUserId(req.user._id, function (brainstorms) {
				res.render('brainstorms/index', {
					brainstorms:brainstorms, layout:"../layout"
				})
			})			
		}
        // строка вставлена временно вызов откправки сообщения
        //emailer.sendmail(['kom_76@mail.ru','mouse@gmail.com'],'Текст\r\nдруже входи на штурм \r\n');
	});
  // GET /brainstorms/new
  // Создаем шторм для заполнения
  // Шаблон new
	app.get('/brainstorms/new', function(req, res){
		if (!req.user) res.redirect('/');
		Brainstorm.new_(function (brainstorm) {
			res.render('brainstorms/new', {
				brainstorm:brainstorm, layout:"../layout"
			})
		})
	});
  // POST /brainstorms
  // Записываем параметры нового шторма в БД
  // Шаблон show
	app.post('/brainstorms', function(req, res){
		if (!req.user) res.redirect('/');
		req.body.brainstorm.user = req.user;
		Brainstorm.create(req.body.brainstorm, function (brainstorm) {
			res.redirect('/brainstorms/' + brainstorm.link);
		})
	});
  // GET /brainstorms/:link
  // Получаем данные конкретного шторма
  // Шаблон show
	app.get('/brainstorms/:link', function(req, res){
		Brainstorm.find(req.params.link, function (brainstorm) {
			var isChat = (new Date().getTime() - brainstorm.start_at.getTime() < brainstorm.duration * 60000)
			//Если приватный
			if (!brainstorm["public"]) {
				if ((isChat)||(req.user !== undefined)&&(req.user.id == brainstorm.user)) {
					brainstorm.countdown = (brainstorm.duration * 60000) - (new Date().getTime() - brainstorm.start_at.getTime())
					res.render('brainstorms/show', {
						brainstorm:brainstorm, layout:"../layout", isChat:isChat
					})
				} else {
					res.redirect('/');
				}
			} else {
				brainstorm.countdown = (brainstorm.duration * 60000) - (new Date().getTime() - brainstorm.start_at.getTime())
				res.render('brainstorms/show', {
					brainstorm:brainstorm, layout:"../layout", isChat:isChat
				})
			}
		})
	});
  // GET /brainstorms/:link/edit
  // Выбираем шторм для редактирования (будет недоступно пользователю)
  // Шаблон show
	app.get('/brainstorms/:link/edit', function(req, res){
		if (!req.session.admin) res.redirect('/') 
		Brainstorm.find(req.params.link, function (brainstorm) {
			res.render('brainstorms/edit', {
				brainstorm:brainstorm, layout:"../layout"
			})
		})
	});
  // PUT /brainstorms/:link
  // Записываем изменения шторма в БД (будет недоступно пользователю)
  // Шаблон show
	app.put('/brainstorms/:link', function(req, res){
		if (!req.session.admin) res.redirect('/') 
		Brainstorm.update(req.body.brainstorm, function (brainstorm) {
			res.render('brainstorms/show', {
				brainstorm:req.body.brainstorm, layout:"../layout"
			})
		})
	});
  // POST /brainstorms/:link/comment
  // Добавляем комментарий к завершенному брейншторму
  // Шаблон show
	app.post('/brainstorms/:link/comment', function(req, res){
		Brainstorm.find(req.params.link, function (brainstorm) {
			if ((!brainstrom['public'])&&(req.user !== undefined)&&(req.user.id == brainstorm.user)) {
				Brainstorm.addComment(req.params.link, req.body.comment, function (brainstorm) {
					res.redirect('/brainstorms/' + brainstorm.link);
				})				
			} else {
				 res.redirect('/');
			}
		})
	});
  // GET /brainstorms/:link/delete
  // Удаляем шторм
  // Шаблон index
	app.get('/brainstorms/:link/delete', function(req, res){
		Brainstorm.find(req.params.link, function (brainstorm) {
			if ((req.session.admin)||(req.user !== undefined)&&(req.user.id == brainstorm.user)) {
				Brainstorm.destroy(req.params.link, function () {
					res.redirect('/brainstorms');
				})
			} else {
				 res.redirect('/');
			}
		})
	});
}());
