app = require("../app");

var mongoose = require('mongoose');
var util = require('util');
    
var Schema = mongoose.Schema
  , ObjectId = Schema.ObjectId;
  
var UserSchema = new Schema({
	"uid":{type:String},  		// По этому полю происходит поиск
	"name":{type:String},       // Отображается в приглашении и чате
	"network":{type:String},    // Какая социальная сеть используется
	"profile":{}    			// Данные полученные из соц сети
});


mongoose.model('User', UserSchema);

var User = mongoose.model('User');

var exp = function () {
	this.all = function (callback) {
		User.find(function (err, users) {
		  if (err) throw err;
		  callback(users);
		}); 
	};
	this.new_ = function (callback) {
		 callback(new User());
	};
	
	this.find = function (link, callback) {
		User.find({link:link}, function (err, users) {
		  if (err) throw err;
		  callback(users[0]);
		}); 
	};
	this.findOne = function (_id, callback) {
		User.find({_id:_id}, function (err, users) {
		  if (err) throw err;
		  callback(users[0]);
		}); 
	};
	this.findOrCreateByUidAndNetwork = function(uid, network, profile, promise) {
		User.find({uid: uid, network: network}, function(err, users) {
			if(err) throw err;
			if(users.length > 0) {
				promise.fulfill(users[0]);
				console.log(promise);
			} else {
				var user = new User();
				user.network = network;
				user.uid = uid;
				user.profile = profile;
				user.name = profile.first_name || profile.name;
				user.save(function(err) {
					if (err) throw err;
					promise.fulfill(user);
				});
			}
		});
	};

	this.create = function (user_, callback) {
		var user = new User();
		for (var prop in user_) {
			user[prop] = user_[prop];
		};
		user.save(function (err) {
		    if (err) throw err;
		    callback(user_);
		});
	};
	this.update = function (user, callback) {
		var _id = user._id;
		delete user._id;
	    User.update({_id:_id}, user,  {multi: true}, function (err) {
		    if (err) throw err;
		    user._id = _id;
			callback(user);
		});
	}
	this.destroy = function (link, callback) {
		User.remove({link:link}, function (err) {
			if (err) throw err;
			callback();
		}); 
	}
}
module.exports = new exp();
