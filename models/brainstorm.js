app = require("../app");

var mongoose = require('mongoose');
    
var Schema = mongoose.Schema
  , ObjectId = Schema.ObjectId;


var Comment = new Schema({
    text      : String
  , likes     : Number
  , name      : String
  , date      : Date
  , id		  : Number
});

var Member = new Schema({
    name      : String
  , link	  : String
  , memberId  : Number
});

BrainstormSchema = new Schema({
    //Ссылка на брейншторм
	"link":{type:String, unique:true},
	//Тема брейншторма
	"title":{type:String},
	//Время старта брейншторма
	"start_at":{type:Date},
    //Длительность в секундах
    "duration":{type:Number},
	//Является ли активных или уже завершен
	"active":{type:Boolean},
    //Является ли шторм публичным или по завершению закрывается от мира
    "public":{type:Boolean},
    //Комментарии из обсуждения	
	"comments":[Comment],
	//Имена участников
	"members":[Member],
	//Инициатор
	"user":{type:ObjectId}
});

var randomHash = (function () {
    var letters = 'AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz1234567890';
    return function (len) {
        var result = '';
        for (var i=0; i < len; i++) {
            result += letters[Math.floor(Math.random() * letters.length)]
        }
        return result;
    }
})();

mongoose.model('Brainstorm', BrainstormSchema);

var Brainstorm = mongoose.model('Brainstorm');

var exp = function () {
	this.all = function (callback) {
		Brainstorm.find(function (err, brainstorms) {
		  if (err) throw err;
		  callback(brainstorms);
		}); 
	};
	this.findByUserId = function (userId, callback) {
		Brainstorm.find({user:userId}, function (err, brainstorms) {
		  if (err) throw err;
		  callback(brainstorms);
		}); 
	};
	this.new_ = function (callback) {
	     var brainstorm = new Brainstorm();
		 callback(brainstorm);
	};
	this.find = function (link, callback) {
		Brainstorm.find({link:link}, function (err, brainstorms) {
		  if (err) throw err;
		  callback(brainstorms[0]);
		}); 
	};
	this.create = function (brainstorm_, callback) {
		console.dir(brainstorm_);
		var brainstorm = new Brainstorm();
		for (var prop in brainstorm_) {
			brainstorm[prop] = brainstorm_[prop];
		};
		brainstorm.duration = +brainstorm_.duration || 30;
        brainstorm.link = "br" + randomHash(40);
        brainstorm.start_at = new Date();
		brainstorm.save(function (err) {
		    if (err) throw err;
		    callback(brainstorm);
		});
	};
	this.saveChat = function (brainstorm_, callback) {
	    Brainstorm.findById({_id:brainstorm_._id}, function (err, brainstorm) {
	    	brainstorm.comments = brainstorm_.comments;
	    	brainstorm.members = brainstorm_.members;
		    if (err) throw err;
			brainstorm.save(function (err) {
		    	if (err) throw err;
		    	callback(brainstorm);
			});
		});
	}
	this.addComment = function (link, comment, callback) {
	    Brainstorm.findOne({link:link}, function (err, brainstorm) {
	    	comment.date = new Date();
	    	if (brainstorm.comments && brainstorm.comments.length > 0) {
		    	brainstorm.comments.push(comment);    		
	    	} else {
	    		brainstorm.comments = [comment];
	    	}
		    if (err) throw err;
			brainstorm.save(function (err) {
		    	if (err) throw err;
		    	callback(brainstorm);
			});
		});
	}
	this.destroy = function (link, callback) {
		Brainstorm.remove({link:link}, function (err) {
			if (err) throw err;
			callback();
		}); 
	}
}
module.exports = new exp();
