/**
 * Вспомогательные функции. Доступны через глобальный объект Utils
 * @author asci@yandex.ru
 */
if (window.Utils === undefined){
	Utils = window.Utils = {};
}
/**
 * Экранирование строки от тегов
 * @param String txt - неэкранированный текст
 * @return String
 */
Utils.escape = function(txt) {
   result = txt.replace(/&/g, '&amp;')
           .replace(/</g, '&lt;')
           .replace(/>/g, '&gt;')
           .replace(/"/g, '&quot;')
   return result;
}
/**
 * Генерация числобуквенной случайной последовательности заданной длинны
 * @param int len - длинна последовательности в символах
 * @return String
 */
Utils.randomHash = (function () {
    var letters = 'AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz1234567890';
    return function (len) {
        var result = '';
        for (var i = 0; i < len; i++) {
            result += letters[Math.floor(Math.random() * letters.length)]
        }
        return result;
    }
})();
/**
 * Перевод переданных секунд в строку вида hH:MM:SS
 * @param int time - время в секундах
 * @return String
 */
Utils.getStringTime = function(time) {
  		var hours = Math.floor(time / 60 / 60);
		var minutes = Math.floor((time - (hours * 60 * 60)) / 60);
		var seconds = Math.floor(time - (hours * 60 * 60) - (minutes * 60));
		return hours + ':' + ((minutes > 9) ? '' + minutes : '0' + minutes) + ':' + ((seconds > 9) ? '' + seconds : '0' + seconds);
	}
/**
 * Установка куки
 * @param String name - имя куки, обязательно
 * @param * value - хранимое значение, обязательно
 * @param Date expires - дата, до которой действует кука
 * @param String path - путь к куке
 * @param String domain - к какому домену относиться
 * @param Boolean secure - защита куки
 * @return Boolean - если кука установлена успешно
 */
Utils.setCookie = function(name, value, expires, path, domain, secure) {
    if (!name || !value) return false;
    var str = name + '=' + encodeURIComponent(value);
     
    if (expires) str += '; expires=' + expires.toGMTString();
    if (path)    str += '; path=' + path;
    if (domain)  str += '; domain=' + domain;
    if (secure)  str += '; secure';
     
    document.cookie = str;
    return true;
}
/**
 * Получение значения куки
 * @param String name - имя целевой куки
 * @return String - в случае успеха
 */
Utils.getCookie = function(name) {
    var pattern = "(?:; )?" + name + "=([^;]*);?";
    var regexp  = new RegExp(pattern);
     
    if (regexp.test(document.cookie))
    return decodeURIComponent(RegExp["$1"]);
     
    return false;
}
/**
 * Удаление установленной куки
 * @param String name - имя куки
 * @param String path - путь
 * @param String domain - домен
 * @return Boolean - в случае успеха
 */
Utils.deleteCookie = function(name, path, domain) {
    return Utils.setCookie(name, null, new Date(0), path, domain);
}