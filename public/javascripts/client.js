$(document).ready(function () {
	//Базовые данные пользователя
	var user = 	{
			likedIds:[]
		};
		
	var members = []
		,messages = []
		,isActiveTab = true
		,startTitle = document.title
		,newMessagesCount = 0
    var socket = io.connect('/');
	
    //Зашел новый участник - добавляем в список и пишем сообщение в ленту
    socket.on('member', function (data) {
    	members.push(data);
    	var head = '<div class="goneMessage">';
    	var body = "<p><b>" + data.name + '</b> присоединился к обсуждению</p>';
    	var end = '</div>'
    	$('#messages').append(head + body + end);
    	$('#members').append("<p id=" + data.id + "member>" + data.name + "</p>");
    	recalcMostLiked();
    });
    //Зашел новый участник - добавляем в список и пишем сообщение в ленту
    socket.on('return', function (data) {
    	var head = '<div class="goneMessage">';
    	var body = "<p><b>" + data.name + '</b> вернулся</p>';
    	var end = '</div>'
    	$('#messages').append(head + body + end);
    	$('#members p#' + data.id + "member").text(data.name);
    	$('#members p#' + data.id + 'member').toggleClass('gray');
   });
    //При подключении получаем всех участников и кидаем их в список участников, без оповещения в ленте
    socket.on('members', function (data) {
    	members.push(data);
    	$('#members').append("<p id=" + data.id + "member>" + data.name + "</p>");
    	recalcMostLiked();
    });
    //Участник покинул штурм - удаляем его из списка участников и пишем сообщение в ленту
    socket.on('gone', function (data) {
    	var head = '<div class="goneMessage">';
    	var body = "<p> Учаcтник <b>" + data.name + '</b> вышел</p>';
    	var end = '</div>'
    	$('#messages').append(head + body + end);
    	$('#members p#' + data.id + 'member').text(data.name + " вышел");
    	$('#members p#' + data.id + 'member').toggleClass('gray');
    	recalcMostLiked();
    });
    //Штурм окончен - блокируем сокет и изменяем форму отправки сообщения
    socket.on('end', function (data) {
    	socket.disconnect();
    	socket = null;
    	$(".like").fadeOut('fast');
    	var head = '<div id="endMessage"><h2>Брейншторм окончен</h2>';
    	var body = '<p>Если же вы хотите что-то добавить, нажмите <a href="" id="addForm">сюда</a></p></div>'
    	$('#sendForm')[0].innerHTML = head + body;
    	$("#counter").hide();
    	recalcMostLiked();
    });    
    //Новое сообщение - добавляем в ленту
    socket.on('message', function (data) {
    	messages[data.id] = data;
    	var head = '<div class="comment" id="'+ data.id + 'msg">';
    	var name = '<p class="commentName">' + Utils.escape(data.name) + '</p>';
    	var date = '<p class="commentDate">' + new Date(data.date).toLocaleTimeString() + '</p>';
    	var text = '<p class="commentText">' + Utils.escape(data.text) + '</p>';
    	var likes = '<div class="commentLikes"><input id="' + data.id + 'like" type="button" value="Нравится" class="like">' + '<p>' + data.likes + '</p></div>'
    	var end = '<div class="clear"></div></div>';
        $('#messages').append(head + name + date + text + likes + end);
        $('#'+ data.id +'like').click(sendLike);
        alarmTitle();
    });

    //Кому-то понравилось сообщение - увеличиваем счетчик
    socket.on('like', function (data) {
    	messages[data.id].likes = data.likes;
    	recalcMostLiked();
        $('#' + data.id + 'msg .commentLikes p').text(data.likes);
    });
    //Пришли лайки данного пользователя
    socket.on('liked', function (data) {
    	user.likedIds = data.likedIds;
    	for (var i = 0; i < data.likedIds.length; i++){
    		$("#" + data.likedIds[i] + 'like').hide();
    	}
    });
    socket.on('multi', function (data){
    	socket.disconnect();
    	socket = null;
    	var head = '<div id="endMessage"><h2>Повторный вход</h2>';
    	var body = '<p>К сожалению, вы не можете одновременно дважды войти в обсуждение</p></div>'
    	$('#sendForm')[0].innerHTML = head + body;
    	$("#counter").hide();    	
    })
    //Меняем заголовок, если пришли новые сообщения и окно не в фокусе
    var alarmTitle = function  () {
      if (isActiveTab) return;
      newMessagesCount++;
      document.title = '(' + newMessagesCount + ')' + startTitle;
    }
    //Перерисовываем хорошие идеи в связи с различными событиями (иногда надо удалять идеи)
    var recalcMostLiked = function () {
    	$('#mostLiked')[0].innerHTML = '';
    	for (var id in messages) {
    		if (messages[id].likes < (members.length / 2 << 0)) continue;
	    	var head = '<div class="comment">';
	    	var name = '<p class="commentName">' + Utils.escape(messages[id].name) + '</p>';
	    	var date = '<p class="commentDate">' + new Date(messages[id].date).toLocaleTimeString() + '</p>';
	    	var text = '<p class="commentText">' + Utils.escape(messages[id].text) + '</p>';
	    	var likes = '<div class="commentLikes"><p>' + messages[id].likes + '</p></div>'
	    	var end = '<div class="clear"></div></div>';
	        $('#mostLiked').append(head + name + date + text + likes + end);
    	}
    }
    //Кидаем базовую информацию для идетификации сокета
    var authSocket = function () {
      var link = $("#authLink").text();
      var name = prompt('Ваше имя:', '');
      var sid = Utils.getCookie('sid');
      if (name != '') {
      	socket.emit('auth', {link:link, name:Utils.escape(name), sid:sid});
      } else {
      	authSocket();
      }
    }
    //Отправка сообщения на сервер
    var sendMessage = function () {
    	var txt = $('#text').val();
		if ((String(txt).length > 0)&&(/\S/.test(txt))) {
            socket.emit('message', {text: Utils.escape(txt)});
            $('#text').val('');
		}
    }
    //Посылаем лайк на сервер
    var sendLike = function () {
    	var id = parseInt(this.id);
        if (id != NaN) {
        	if (user.likedIds.indexOf(id) !== -1) return;
            socket.emit('like', {id: id});
            user.likedIds.push(id);
            $(this).fadeOut('fast');
        }
    }
    //Обработка нажатий на текстовом поле
    $('#text').keydown(function (e) {
		if (e.keyCode == 13) {
			sendMessage();
		}
	});

    $('#send').click(sendMessage);

	window.onfocus = function(){
		newMessagesCount = 0;
		isActiveTab = true;
		document.title = startTitle;
	}
	
	window.onblur = function(){
		isActiveTab = false;
	}

    var init = function () {
        if (socket !== undefined){
      	    authSocket();
	        var startCountDown = Math.round(+($("#countdownHide").text()) / 1000);
	        var countDownTimer;
	        setInterval(function  () {
	            $("#countdown").text(Utils.getStringTime(startCountDown--));
	        }, 1000);
        }
    }
    
    init();
});
