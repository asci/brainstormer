/**
 * Module dependencies.
 */

var express = require('express')
	, everyauth = require('everyauth')
	, Promise = everyauth.Promise
	, util = require('util')
	, fs = require('fs')
	, mongoose = require('mongoose')
	, form = require('connect-form')
	, routes = require('./routes')
	, _ = require('underscore')
	
	//Models
	, User = require('./models/user')
	, Brainstorm = require('./models/brainstorm')
	, Schema = mongoose.Schema
	, ObjectId = Schema.ObjectId

mongoose.connect('mongodb://127.0.0.1/base');

// Everyauth settings
everyauth.everymodule.findUserById(function(id, callback) {
	User.findOne(id, function(user) {
		callback(null, user);
	});
});

everyauth.twitter.consumerKey('swX2SpcrkIjIh0nfWzQVQw').consumerSecret('Tdeey3ZsDBBxm5K98s0gddz7oPRAmkzwEWYZvGaH28').findOrCreateUser(function(session, accessToken, accessTokenSecret, twitterUserData) {
	var promise = this.Promise();
	User.findOrCreateByUidAndNetwork(twitterUserData.id, 'twitter', twitterUserData, promise);
	return promise;
}).redirectPath('/brainstorms');

everyauth.facebook.appId("119306361502312").appSecret("c1f4cb753fa6d272c6b53801355f3f45").findOrCreateUser(function(session, accessToken, accessTokenExtra, fbUserMetadata) {
	var promise = this.Promise();
	User.findOrCreateByUidAndNetwork(fbUserMetadata.id, 'facebook', fbUserMetadata, promise);
	return promise;
}).redirectPath('/brainstorms');

everyauth.vkontakte.appId('2756227').appSecret('bt9NT8P5VIhOamEXpoVz').findOrCreateUser(function(session, accessToken, accessTokenSecret, vkUserData) {
	var promise = this.Promise();
	User.findOrCreateByUidAndNetwork(vkUserData.uid, 'vkontakte', vkUserData, promise);
	return promise;
}).redirectPath('/brainstorms');

var app = module.exports = express.createServer();

// Configuration

app.configure(function() {
	app.set('views', __dirname + '/views');
	app.set('view engine', 'jade');
	app.use(express.bodyParser());
	app.use(express.cookieParser());
	app.use(express.session({
		secret: 'asd34r3c234v234'
	}));
	app.use(everyauth.middleware());
	app.use(express.methodOverride());
	app.use(app.router);
	app.use(express['static'](__dirname + '/public'));
});

everyauth.helpExpress(app);
app.dynamicHelpers({
	currentUser: function(req, res) {
		return req.user;
	},
	sessionId: function(req, res) {
		return req.sessionID;
	}
})
everyauth.debug = false;

app.configure('development', function() {
	app.use(express.errorHandler({
		dumpExceptions: true,
		showStack: true
	}));
});

app.configure('production', function() {
	app.use(express.errorHandler());
});

// Routes

app.get('/', routes.index);
require("./controllers/user");
emailer=require('./controllers/emailer');
require("./controllers/brainstorm");

app.listen(80);

var chats = [];

console.log("Express server listening on port %d in %s mode", app.address().port, app.settings.env);

var io = require('socket.io').listen(app);
io.sockets.on('connection', function(socket) {
	console.log('someone connected');
	socket.on('auth', function(data) {
		console.log('someone auth');
		//Создаем чатик если его не было, и вешаем обработчик оканчания чатика
		if(chats[data.link] === undefined) {
			//Тут назначаем главным того, кто первый зашел
			chats[data.link] = {
				members: [],
				messages: [],
				initiator: data.sid
			};
			Brainstorm.find(data.link, function(brainstorm) {
				console.log('Brainstorm was finded');
				var countdown = (brainstorm.duration * 60000) - (new Date().getTime() - brainstorm.start_at.getTime())
				chats[data.link].countdown = 0;
				setTimeout(function() {
					brainstorm.comments = chats[data.link].messages;
					brainstorm.members = _.map(chats[socket.member.link].members, function(member) {
						return {
							name: member.name,
							link: member.link,
							memberId: member.id
						};
					});
					console.log('Prepare to save');
					Brainstorm.saveChat(brainstorm, function(b) {
						console.log('Saved successfuly');
						var i, len;
						len = chats[socket.member.link].members.length;
						for (i = 0; i < len; i++) {
							console.log('emit end' + data.text);
							if(!chats[socket.member.link].members[i].legal) continue;
							chats[socket.member.link].members[i].socket.emit('end', {
								text: 'Брейншторм окончен'
							});
							//delete chats[socket.link]
						}
					});
				}, countdown + 1000);
			});
			createChatMember(socket, data);
		//Если чатик есть, то обновляем или добавляем пользователя
		} else {
			//Если брейншторм уже есть, то проходимся по всем его участникам и ищем
			//участника с такой же сессией как и у текущего сокета
			//Если такой участник уже есть и он активен (легален), то прерываем текущий сокет
			//в другом случае устанавливаем в сокетом найденному участнику текущий сокет, и посылаем
			//текущему сокету какие сообщения были заплюсены
			var member = _.find(chats[data.link].members, function (m){
				return m.sid == data.sid
			});
			//Нашли участника
			if (member !== undefined) {
				//Если активен - закрываем этот сокет
				if (member.legal) {
					socket.emit('multi', {
						text: 'Множественный вход'
					});
					socket.disconnect();
				//Если был закрыт - обновлем сокет и делаем активным и говорим всем - "ОН ВЕРНУЛСЯ!!!"
				} else {
					member.socket = socket;
					member.legal = true;
					socket.member = member;
					//Присылаем новому участнику все сообщения
					var len = chats[member.link].messages.length, i;
					for (i = 0; i < len; i++) {
						socket.emit('message', chats[member.link].messages[i]);
					}
					len = chats[member.link].members.length;
					for (i = 0; i < len; i++) {
						chats[member.link].members[i].socket.emit('return', {
							name: member.name,
							id: member.id,
							legal: true
						});
					};
					member.socket.emit('liked', {likedIds: member.likedIds});
				}
			} else {
				createChatMember(socket, data);
			}
		}
	});
	socket.on('message', function(data) {
		var member = socket.member;
		if ((!member) || (!member.legal)) {
			console.log('illegal user');
			return;
		}
		console.log('someone send message');
		var len = chats[member.link].members.length;
		console.log(len);
		chats[member.link].messages.push({
			name: member.name,
			text: data.text,
			date: new Date(),
			likes: 0,
			id: chats[member.link].messages.length
		});
		for(var i = 0; i < len; i++) {
			console.log('emit message' + data.text);
			chats[member.link].members[i].socket.emit('message', _.last(chats[member.link].messages));
		};
	});
	socket.on('like', function(data) {
		var member = socket.member, i;
		if ((!member) || (!member.legal)) {
			console.log('illegal user');
			return;
		}
		console.log('someone like message');
		//Если уже лайкали - прерываем
		if (member.likedIds.indexOf(data.id) !== -1) {
			console.log('repeat liking');
			return;
		}
		//Иначе делаем лайк
		var msg = _.find(chats[member.link].messages, function(m) {
			return m.id == data.id
		});
		member.likedIds.push(data.id);
		var likes = ++msg.likes;
		var len = chats[member.link].members.length;
		for (i = 0; i < len; i++) {
			console.log('emit like ' + likes);
			chats[member.link].members[i].socket.emit('like', {
				id: data.id,
				likes: likes
			});
		}
	});
	socket.on('disconnect', function() {
		var member = socket.member, i;
		if ((!member) || (!member.legal)) {
			console.log('illegal user');
			return;
		}
		//Делаем обманку-заглушку
		member.socket.emit = function(e, t) {return};
		member.legal = false;		
		console.log('someone disconnected');
		var len = chats[member.link].members.length;
		for (i = 0; i < len; i++) {
			chats[member.link].members[i].socket.emit('gone', {
				name: member.name,
				id: member.id,
				legal: false
			});
		}
	});
});


var createChatMember = function (socket, data) {
	//Создаем нового участника
	var member = {
		link: data.link,
		name: data.name,
		sid: data.sid,
		socket: socket,
		likedIds:[],
		legal: true
	};
	//Делаем обратную ссылку
	socket.member = member;
	
	var i, len;
	len = chats[member.link].messages.length;
	//Присылаем новому участнику все сообщения
	for (i = 0; i < len; i++) {
		socket.emit('message', chats[member.link].messages[i]);
	}
	//Присылаем всех участников, кроме нового и вышедших
	len = chats[member.link].members.length;
	for (i = 0; i < len; i++) {
		if (!chats[member.link].members[i].legal) continue;
		socket.emit('members', {
			legal: chats[member.link].members[i].legal,
			id: chats[member.link].members[i].id,
			name: chats[member.link].members[i].name
		});
	}
	//Оповещаем всех участников, в том числе и себя, что пришел новый участник
	chats[data.link].members.push(member);
	member.id = len++;
	for (i = 0; i < len; i++) {
		if (!chats[member.link].members[i].legal) continue;
		console.log('emit message' + data.text);
		chats[member.link].members[i].socket.emit('member', {
			id: member.id,
			name: member.name
		});
	}
}